#-*- coding: utf-8 -*-

import numpy as np
import keras
from keras import layers, models, datasets
from keras import backend as K
from keras.utils import np_utils
import matplotlib.pyplot as plt
from sklearn import model_selection, metrics
from sklearn.preprocessing import MinMaxScaler
import os

class CNN(models.Model):
    def __init__(self, nb_classes, in_shape=None):
        self.nb_classes = nb_classes
        self.in_shape = in_shape
        self.build_model()
        super().__init__(self.x, self.y)
        self.compile()

    def build_model(self):
        nb_classes = self.nb_classes
        in_shape = self.in_shape

        x = layers.Input(in_shape)

        h = layers.Conv2D(32, (3, 3), activation='relu', input_shape=in_shape)(x)
        h = layers.Conv2D(64, (3, 3), activation='relu')(h)
        h = layers.MaxPooling2D(pool_size=(2, 2))(h)
        h = layers.Dropout(0.25)(h)
        h = layers.Flatten()(h)
        z_cl = h

        h = layers.Dense(128, activation='relu')(h)
        h = layers.Dropout(0.5)(h)
        z_fl = h

        y = layers.Dense(nb_classes, activation='softmax', name='preds')(h)

        self.cl_part = models.Model(x, z_cl)
        self.fl_part = models.Model(x, z_fl)

        self.x, self.y = x, y

    def compile(self):
        models.Model.compile(self, loss='categorical_crossentropy',
        optimizer='adadelta', metrics=['accuracy'])

class DataSet:
    def __init__(self, x, y, nb_classes, scaling=True,
                test_size=0.2, random_state=0):
        self.x = x
        self.add_channels()

        x = self.x

        x_train, x_test, y_train, y_test = model_selection.train_test_split(
            x, y, test_size=test_size, random_state=random_state)

        print(x_train.shape, y_train.shape)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')

        if scaling:
            scaler = MinMaxScaler()
            n = x_train.shape[0]
            x_train = scaler.fit_transform(x_train.reshape(n, -1)).reshape(x_train.shape)
            n = x_test.shape[0]
            x_test = scaler.fit_transform(x_test.reshape(n, -1)).reshape(x_test.shape)

        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')
        
        self.y_test_label = y_test

        y_train = np_utils.to_categorical(y_train, nb_classes)
        y_test = np_utils.to_categorical(y_test, nb_classes)

        self.x_train, self.x_test = x_train, x_test
        self.y_train, self.y_test = y_train, y_test

    def add_channels(self):
        x = self.x

        if len(x.shape) == 3:
            N, img_rows, img_cols = x.shape

            if K.image_dim_ordering() == 'th':
                x = x_reshape(x.shape[0], 1, img_rows, img_cols)
                input_shape = (1, img_rows, img_cols)
            else:
                x = x_reshape(x.shape[0], img_rows, img_cols, 1)
                input_shape = (img_rows, img_cols, 1)
        else:
            input_shape = x.shape[1:]

        self.x = x
        self.input_shape = input_shape

class Machine():
    def __init__(self, x, y, nb_classes=2, fig=True):
        self.nb_classes = nb_classes
        self.set_data(x, y)
        self.set_model()
        self.fig = fig

    def set_data(self, x, y):
        nb_classes = self.nb_classes
        self.data = DataSet(x, y, nb_classes)
        print('data.input_shape:', self.data.input_shape)

    def set_model(self):
        nb_classes = self.nb_classes
        data = self.data
        self.model = CNN(nb_classes=nb_classes, in_shape=data.input_shape)

    def fit(self, epochs=10, batch_size=128, verbose=2):
        data = self.data
        model = self.model

        history = model.fit(data.x_train, data.y_train,
            batch_size=batch_size, epochs=epochs, verbose=verbose,
            validation_data=(data.x_test, data.y_test))

        return history

    def run(self, epochs=20, batch_size=128, verbose=2):
        data = self.data
        model = self.model
        fig = self.fig

        history = self.fit(epochs=epochs, batch_size=batch_size, verbose=verbose)

        print('Confusion matrix')
        y_test_pred = model.predict(data.x_test, verbose=verbose)
        y_test_label = np.argmax(y_test_pred, axis=1)
        print(metrics.confusion_matrix(data.y_test_label, y_test_label))
        
        performance_test = model.evaluate(data.x_test, data.y_test,
            batch_size=batch_size, verbose=verbose)

        print('Performance:', performance_test)

        if fig:
            plt.figure(figsize=(12, 4))
            plt.subplot(1, 2, 1)
            plot_acc(history)
            plt.subplot(1, 2, 2)
            plot_loss(history)
            plt.show()

        self.history = history

def plot_loss(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc=0)

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc=0)

